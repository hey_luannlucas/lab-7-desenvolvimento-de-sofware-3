# Spring Security App 🛡️

Este é um exemplo simples de uma aplicação Spring Boot com Spring Security.

## Descrição ℹ️

Este projeto é uma aplicação Spring Boot básica com autenticação e autorização usando Spring Security. Ele contém:

- Autenticação baseada em banco de dados com criptografia de senha usando BCrypt.
- Autorização com base em funções (roles).
- Páginas de acesso para usuários autenticados e diferentes páginas para usuários com diferentes funções (ROLE_ADMIN e ROLE_USER).
- Um endpoint de registro para criar novos usuários.

## Configuração ⚙️

A aplicação utiliza um banco de dados Mysql que será executado em um contêiner Docker. O Docker irá iniciar o banco de dados e a tabela, tudo o que você precisa fazer é garantir que o Docker esteja em execução ao iniciar a aplicação Spring.

## Endpoints 🛣️

Gerenciamento de acesso:
  
<p align="center">
      <img src="/SpringSecurityApp/assets/diagram.png" alt="login" width="800">
    </p>

- `/home`: Página de boas-vindas acessível a todos.
- `/admin/home`: Página inicial para usuários com a função ROLE_ADMIN.
- `/user/home`: Página inicial para usuários com a função ROLE_USER.
- `/login`: Página de login personalizada.

## Tecnologias utilizadas 🚀

- **Spring Boot:** Framework para criação de aplicações Java baseadas em Spring.
- **Spring Security:** Framework de autenticação e autorização para aplicações Java.
- **Spring Data JPA:** Facilita o acesso aos dados do banco de dados relacional, simplificando as operações de CRUD.
- **BCrypt:** Algoritmo de criptografia utilizado para criptografar senhas.
- **Hibernate:** Framework de mapeamento objeto-relacional para Java.
- **Mysql Database com docker:** Banco de dados para desenvolvimento e testes.
- **thymeleaf:** Uma engine de template moderna, que permite aos criar páginas da web em HTML de forma simples no spring.

## Estrutura do projeto 📁

- `com.example.jala.university.SpringSecurityApp.config`: Configurações de segurança.
- `com.example.jala.university.SpringSecurityApp.controller`: Controladores da aplicação.
- `com.example.jala.university.SpringSecurityApp.model`: Modelos da aplicação (entidades e repositórios).
- `com.example.jala.university.SpringSecurityApp`: Classes de configuração e manipuladores de autenticação personalizados.

## Uso ✍️


1. Faça um registro de usuário no endpoint `/register/user`. Abaixo eu crio um usuário chamado Carlos com função de administrador e maria com função user.

    ```
    POST /register/user
    
    {
        "username": "carlos",
        "password": "senhateste",
        "role": "ADMIN"
    }
    ```
    ```
    POST /register/user
    
    {
        "username": "maria",
        "password": "senhatesteuser",
        "role": "USER"
    }
     ```
    
    <p align="center">
      <img src="/SpringSecurityApp/assets/register.png" alt="registro" >
    </p>

2. Voce pode acessar o `/home` e se redirecionar `/login`

  <p align="center">
      <img src="/SpringSecurityApp/assets/home.png" alt="registro" width="800">
    </p>

ou simplesmente acesse o `/login`

  <p align="center">
      <img src="/SpringSecurityApp/assets/login.png" alt="login" width="800">
    </p>

3. Faça login usando as credencias do usario criado e a resposta sera a tela correspondente a sua role.

 <p align="center">
      <img src="/SpringSecurityApp/assets/loginCarlos.png" alt="loginAdmin" width="800">
    </p>

como ele é administrador a tela será a seguinte:

 <p align="center">
      <img src="/SpringSecurityApp/assets/telaAdmin.png" alt="loginAdmin" width="800">
    </p>

ao fazer login com maria (USER):


 <p align="center">
      <img src="/SpringSecurityApp/assets/telaUser.png" alt="loginUser" width="800">
    </p>

caso tente acessar a rota de admin sendo usuario:

 <p align="center">
      <img src="/SpringSecurityApp/assets/forbidden.png" alt="loginUser" width="800">
    </p>

caso tente acessar um endpoint inexistente:

<p align="center">
      <img src="/SpringSecurityApp/assets/notFound.png" alt="loginUser" width="800">
    </p>

***
<div class="profile-link">
  <a href="https://gitlab.com/hey_luannlucas" style="display: flex; align-items: center; text-decoration: none;">
    <img src="/SpringSecurityApp/assets/icons8-gitlab.svg" alt="GitLab" width="60" height="60" style="margin-right: 10px; border-radius: 50%;">
    <span class="name" style="font-size: 24px; font-weight: bold; color: #0000f;">Luann Lucas</span>
  </a>
</div>
